from adders import *

if __name__ == "__main__":
    units = []
    fa = FullAdder(name="test")
    units.append(fa)

    def ticktock():
        for u in units:
            u.tick()
        for u in units:
            u.tock()


    for _ in range(1000):
        ticktock();

    def p(num):
        print(" --- # {:<3} --- ".format(num))
