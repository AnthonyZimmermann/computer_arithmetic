#!/usr/bin/env python3
import numpy as np
import sys

num = sys.argv[1]
print(num)

if __name__ == "__main__":
    if num.startswith("0b"):
        mode = "binary"
        num = num[2:]
    else:
        mode = "decimal"

    if mode == "binary":
        if num.startswith("0"):
            out = np.int32(0)
        else:
            out = np.int32(-1)
        for c in num:
            out = out<<1
            if c == "1":
                out = out | 0b1
        print(out)
    else:
        num = np.int32(int(num))
        out = ""
        for i in reversed(range(32)):
            out += "{}".format(num>>i & 0b1)
        first = out[0]
        while out[1] == first:
            out = out[1:]
        print(out)
