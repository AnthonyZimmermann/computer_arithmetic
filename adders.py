from base import Unit, BitVector, Bit
from basicBlocks import FullAdder, FullAdderM1, FullAdderM2

class CRA(Unit):
    def __init__(self, length=32, **kwargs):
        super().__init__(**kwargs)
        if self.id == "_":
            self.id = "CRA"
        self.length = length
# input
        self.va = BitVector(self.length)
        self.vb = BitVector(self.length)
        self.cin = Bit()
# output
        self.vcout = BitVector(self.length)
        self.vsum = BitVector(self.length)

# modules
        self.vfa = []
        for _ in range(self.length):
            self.vfa.append(FullAdder())

        self.update_modules()

    def update_modules(self):
        self.vfa[0].a = self.va[0]
        self.vfa[0].b = self.vb[0]
        self.vfa[0].cin = self.cin
        self.vfa[0].update_modules()
        self.vcout[0] = self.vfa[0].cout
        self.vsum[0] = self.vfa[0].sum

        for i in range(1,self.length):
            self.vfa[i].a = self.va[i]
            self.vfa[i].b = self.vb[i]
            self.vfa[i].cin = self.vcout[i-1]
            self.vfa[i].update_modules()
            self.vcout[i] = self.vfa[i].cout
            self.vsum[i] = self.vfa[i].sum

    def tick(self):
        for fa in self.vfa:
            fa.tick()

    def tock(self):
        for fa in self.vfa:
            fa.tock()

    def __str__(self):
        out = "{:<20}: inputs: a={}, b={}, cin={}\n".format(self.id, self.va, self.vb, self.cin)
        out += "{:<20}: outputs: sum={}, cout={}".format("", self.vsum, self.vcout.v[-1])
        return out

class CRA_MixedGates(Unit):
    def __init__(self, length=32, **kwargs):
        super().__init__(**kwargs)
        if self.id == "_":
            self.id = "CRA"
        self.length = length
# input
        self.va = BitVector(self.length)
        self.vb = BitVector(self.length)
        self.cin = Bit()
# output
        self.vcout = BitVector(self.length)
        self.vsum = BitVector(self.length)

# modules
        self.vfa = []
        for i in range(self.length):
            if i % 2 == 0:
                self.vfa.append(FullAdderM1())
            else:
                self.vfa.append(FullAdderM2())

        self.update_modules()

    def update_modules(self):
        self.vfa[0].a = self.va[0]
        self.vfa[0].b = self.vb[0]
        self.vfa[0].cin = self.cin
        self.vfa[0].update_modules()
        self.vcout[0] = self.vfa[0].cout_not
        self.vsum[0] = self.vfa[0].sum

        for i in range(1,self.length):
            self.vfa[i].a = self.va[i]
            self.vfa[i].b = self.vb[i]
            if i % 2 == 0:
                self.vfa[i].cin = self.vcout[i-1]
            else:
                self.vfa[i].cin_not = self.vcout[i-1]
            self.vfa[i].update_modules()
            if i % 2 == 0:
                self.vcout[i] = self.vfa[i].cout_not
            else:
                self.vcout[i] = self.vfa[i].cout
            self.vsum[i] = self.vfa[i].sum

    def tick(self):
        for fa in self.vfa:
            fa.tick()

    def tock(self):
        for fa in self.vfa:
            fa.tock()

    def __str__(self):
        out = "{:<20}: inputs: a={}, b={}, cin={}\n".format(self.id, self.va, self.vb, self.cin)
        if self.length % 2 == 0:
            out += "{:<20}: outputs: sum={}, cout={}".format("", self.vsum, self.vcout.v[-1])
        else:
            out += "{:<20}: outputs: sum={}, cout_not={}".format("", self.vsum, self.vcout.v[-1])
        return out



if __name__ == "__main__":
    def testCRA():
        cra = CRA_MixedGates(64)
#        cra = CRA(64)

        def ticktock():
            cra.tick()
            cra.tock()

        for _ in range(1000):
            ticktock()

        def p(num):
            print("--- #{:<3} ---".format(num))
            print(cra)

        a = 0b0
        b = (2**64)-1
        cin = 0b1
        result = a + b + cin
        cra.va.set(a)
        cra.vb.set(b)
        cra.cin.v = cin
        cra.update_modules()

        cycles=500
        resultFirstTime = True
        for i in range(cycles):
            if i in range(500,501):
                p(i)
            if resultFirstTime and cra.vsum.get() == result:
                p(i)
                resultFirstTime = False
            ticktock()

        p(cycles)

    testCRA()
