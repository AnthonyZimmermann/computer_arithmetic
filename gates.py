from base import Unit, Bit

class INV(Unit):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.id == "_":
            self.id = "inv"
# input
        self.inp = Bit()
# output
        self.out = Bit()

    def update_modules(self):
        pass

    def tick(self):
        self.out_tmp = 1 - self.inp.v

    def tock(self):
        self.out.v = self.out_tmp

    def __str__(self):
        out = "{:<20}: inputs: inp={}\n".format(self.id, self.inp.v)
        out += "{:<20}: outputs: out={}".format("", self.out.v)
        return out

class NOR(Unit):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.id == "_":
            self.id = "nor"
# input
        self.a = Bit()
        self.b = Bit()
# output
        self.out = Bit()

    def update_modules(self):
        pass

    def tick(self):
        self.out_tmp = 1 - (self.a.v | self.b.v)

    def tock(self):
        self.out.v = self.out_tmp

    def __str__(self):
        out = "{:<20}: inputs: a={}, b={}\n".format(self.id, self.a.v, self.b.v)
        out += "{:<20}: outputs: out={}".format("", self.out.v)
        return out

class OR(Unit):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.id == "_":
            self.id = "or"
# input
            self.a = Bit()
            self.b = Bit()
# output
            self.out = Bit()
# intermediate
            self.t1 = Bit()

# modules
            self.nor = NOR()
            self.inv = INV()

            self.update_modules()

    def update_modules(self):
        self.nor.a = self.a
        self.nor.b = self.b
        self.nor.update_modules()
        self.t1 = self.nor.out

        self.inv.inp = self.t1
        self.inv.update_modules()
        self.out = self.inv.out

    def tick(self):
        self.nor.tick()
        self.inv.tick()

    def tock(self):
        self.nor.tock()
        self.inv.tock()

    def __str__(self):
        out = "{:<20}: inputs: a={}, b={}\n".format(self.id, self.a.v, self.b.v)
        out += "{:<20}: outputs: out={}".format("", self.out.v)
        return out

class NAND(Unit):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.id == "_":
            self.id = "nand"
# input
        self.a = Bit()
        self.b = Bit()
# output
        self.out = Bit()

    def update_modules(self):
        pass

    def tick(self):
        self.out_tmp = 1 - (self.a.v & self.b.v)

    def tock(self):
        self.out.v = self.out_tmp

    def __str__(self):
        out = "{:<20}: inputs: a={}, b={}\n".format(self.id, self.a.v, self.b.v)
        out += "{:<20}: outputs: out={}".format("", self.out.v)
        return out

class AND(Unit):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.id == "_":
            self.id = "and"
# input
        self.a = Bit()
        self.b = Bit()
# output
        self.out = Bit()
# intermediate
        self.t1 = Bit()

# modules
        self.nand = NAND()
        self.inv = INV()

        self.update_modules()

    def update_modules(self):
        self.nand.a = self.a
        self.nand.b = self.b
        self.nand.update_modules()
        self.t1 = self.nand.out

        self.inv.inp = self.t1
        self.inv.update_modules()
        self.out = self.inv.out

    def tick(self):
        self.nand.tick()
        self.inv.tick()

    def tock(self):
        self.nand.tock()
        self.inv.tock()

    def __str__(self):
        out = "{:<20}: inputs: a={}, b={}\n".format(self.id, self.a.v, self.b.v)
        out += "{:<20}: outputs: out={}".format("", self.out.v)
        return out

class XNOR(Unit):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.id == "_":
            self.id = "xnor"
# input
        self.a = Bit()
        self.b = Bit()
# output
        self.out = Bit()
# intermediate
        self.t1 = Bit()

# modules
        self.nand = NAND()
        self.oai21 = OAI21()

        self.update_modules()

    def update_modules(self):
        self.nand.a = self.a
        self.nand.b = self.b
        self.nand.update_modules()
        self.t1 = self.nand.out

        self.oai21.a = self.a
        self.oai21.b = self.b
        self.oai21.c = self.t1
        self.oai21.update_modules()
        self.out = self.oai21.out

    def tick(self):
        self.nand.tick()
        self.oai21.tick()

    def tock(self):
        self.nand.tock()
        self.oai21.tock()

    def __str__(self):
        out = "{:<20}: inputs: a={}, b={}\n".format(self.id, self.a.v, self.b.v)
        out += "{:<20}: outputs: out={}".format("", self.out.v)
        return out

class XOR(Unit):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.id == "_":
            self.id = "xor"
# input
        self.a = Bit()
        self.b = Bit()
# output
        self.out = Bit()
# intermediate
        self.t1 = Bit()

# modules
        self.nor = NOR()
        self.aoi21 = AOI21()

        self.update_modules()

    def update_modules(self):
        self.nor.a = self.a
        self.nor.b = self.b
        self.nor.update_modules()
        self.t1 = self.nor.out

        self.aoi21.a = self.a
        self.aoi21.b = self.b
        self.aoi21.c = self.t1
        self.aoi21.update_modules()
        self.out = self.aoi21.out

    def tick(self):
        self.nor.tick()
        self.aoi21.tick()

    def tock(self):
        self.nor.tock()
        self.aoi21.tock()

    def __str__(self):
        out = "{:<20}: inputs: a={}, b={}\n".format(self.id, self.a.v, self.b.v)
        out += "{:<20}: outputs: out={}".format("", self.out.v)
        return out

class AOI21(Unit):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.id == "_":
            self.id = "aoi21"
# input
        self.a = Bit()
        self.b = Bit()
        self.c = Bit()
# output
        self.out = Bit()

    def update_modules(self):
        pass

    def tick(self):
        self.out_tmp = 1 - ((self.a.v & self.b.v) | self.c.v)

    def tock(self):
        self.out.v = self.out_tmp

    def __str__(self):
        out = "{:<20}: inputs: a={}, b={}, c={}\n".format(self.id, self.a.v, self.b.v, self.c.v)
        out += "{:<20}: outputs: out={}".format("", self.out.v)
        return out

class OAI21(Unit):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.id == "_":
            self.id = "oai21"
# input
        self.a = Bit()
        self.b = Bit()
        self.c = Bit()
# output
        self.out = Bit()

    def update_modules(self):
        pass

    def tick(self):
        self.out_tmp = 1 - ((self.a.v | self.b.v) & self.c.v)

    def tock(self):
        self.out.v = self.out_tmp

    def __str__(self):
        out = "{:<20}: inputs: a={}, b={}, c={}\n".format(self.id, self.a.v, self.b.v, self.c.v)
        out += "{:<20}: outputs: out={}".format("", self.out.v)
        return out

class AOI22(Unit):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.id == "_":
            self.id = "aoi22"
# input
        self.a = Bit()
        self.b = Bit()
        self.c = Bit()
        self.d = Bit()
# output
        self.out = Bit()

    def update_modules(self):
        pass

    def tick(self):
        self.out_tmp = 1 - ((self.a.v & self.b.v) | (self.c.v & self.d.v))

    def tock(self):
        self.out.v = self.out_tmp

    def __str__(self):
        out = "{:<20}: inputs: a={}, b={}, c={}, d={}\n".format(self.id, self.a.v, self.b.v, self.c.v, self.d.v)
        out += "{:<20}: outputs: out={}".format("", self.out.v)
        return out

class AO22(Unit):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.id == "_":
            self.id = "ao22"
# input
        self.a = Bit()
        self.b = Bit()
        self.c = Bit()
        self.d = Bit()
# output
        self.out = Bit()
# intermediate
        self.t1 = Bit()

# modules
        self.aoi22 = AOI22()
        self.inv = INV()

        self.update_modules()

    def update_modules(self):
        self.aoi22.a = self.a
        self.aoi22.b = self.b
        self.aoi22.c = self.c
        self.aoi22.d = self.d
        self.aoi22.update_modules()
        self.t1 = self.aoi22.out

        self.inv.inp = self.t1
        self.inv.update_modules()
        self.out = self.inv.out

    def tick(self):
        self.aoi22.tick()
        self.inv.tick()

    def tock(self):
        self.aoi22.tock()
        self.inv.tock()

    def __str__(self):
        out = "{:<20}: inputs: a={}, b={}, c={}, d={}\n".format(self.id, self.a.v, self.b.v, self.c.v, self.d.v)
        out += "{:<20}: outputs: out={}".format("", self.out.v)
        return out
