import math
import sys
import time
import random

sineTable = dict()

num_values = 30
delta = math.pi/2/num_values
for i in range(num_values):
    sineTable[delta*i] = math.sin(delta*i)

def sine(val):
    v = val - int(val/2/math.pi)*2*math.pi
    t = int(v/math.pi*2)

    vv = v - int(v/math.pi)*math.pi
    if t == 1:
        vv = math.pi - vv
    elif t == 3:
        vv = math.pi - vv

    x_1, s_x_1 = 0, 0
    x_2, s_x_2 = 0, 0
    for x in sorted(sineTable.keys()):
        x_2 = x
        s_x_2 = sineTable[x_2]
        if vv - x_2 < 0:
            break
        x_1 = x_2
        s_x_1 = s_x_2

    alpha = 0
    if x_2 - x_1 != 0:
        alpha = (vv - x_1) / (x_2 - x_1)

    vv_sine = ((1-alpha) * s_x_1) + (alpha * s_x_2)

    if t == 0 or t == 1:
        return vv_sine
    else:
        return -vv_sine


if __name__ == "__main__":

    def test(num, epsilon=10**-2):
        maxdiff = 0
        for i in range(num):
            x = random.random()*2*math.pi

            x_sine = sine(x)
            x_sine_math = math.sin(x)

            diff = abs(x_sine - x_sine_math)
            maxdiff = max(diff,maxdiff)
            if diff > epsilon:
                print("test failed")
                print("sin({}) = {}".format(x, x_sine))
                print("math.sin({}) = {}".format(x, x_sine_math))
                print("diff = {}".format(abs(x_sine-x_sine_math)))
        return maxdiff

    print("test -> maxdiff={}".format(test(1000)))

    def benchmark_sine(num):
        t0 = time.time()
        for i in range(num):
            x = random.random()*2*math.pi
            x_sine = sine(x)
        return time.time() - t0

    def benchmark_sine_math(num):
        t0 = time.time()
        for i in range(num):
            x = random.random()*2*math.pi
            x_sine_math = math.sin(x)
        return time.time() - t0

    benchmark_iterations = 1000
    # benchmark sine
    sine_time = benchmark_sine(benchmark_iterations)
    print("{} iterations: sine: {}".format(benchmark_iterations, sine_time))
    # benchmark math.sin
    math_sin_time = benchmark_sine_math(benchmark_iterations)
    print("{} iterations: math.sin: {}".format(benchmark_iterations, math_sin_time))
