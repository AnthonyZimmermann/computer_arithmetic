import sys

def newton_iteration(x,p0, f, f_prime, epsilon=2**-31, maxiter=100):
    p = p0
    f_val = f(x, p)
    f_prime_val = f_prime(x, p)
    counter = 0
    while abs(f_val) > epsilon and counter < maxiter:
        p = p - (f_val / f_prime_val)
        f_val = f(x, p)
        f_prime_val = f_prime(x, p)
        counter = counter + 1

    return p,counter


def iteration(x, p0, f, f_prime):
    return p0 - (f/f_prime)


if __name__ == "__main__":

    def f_root(x, p):
        return p*p - x

    def f_p_root(x, p):
        return 2*p

    x = 23464326
    p0 = 10

    if len(sys.argv) > 2:
        x = float(sys.argv[1])
        p0 = float(sys.argv[2])
    elif len(sys.argv) > 1:
        x = float(sys.argv[1])

    root,count = newton_iteration(x, p0, f_root, f_p_root)

    print("sqrt({}) = {}, ({} iterations needed)".format(x,root,count))
    print("{}*{} = {}".format(root,root,root*root))
