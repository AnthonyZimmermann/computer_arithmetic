class Bit(object):
    def __init__(self):
        self.v = 0

    def __str__(self):
        return "{}".format(self.v)

class BitVector(object):
    def __init__(self, length):
        self.length = length
        self.v = []
        for i in range(length):
            self.v.append(Bit())

    def set(self, value):
        for i in range(self.length):
            self.v[i].v = 0b1 & (value>>i)

    def get(self):
        out = 0
        for i in reversed(range(self.length)):
            out = out | (self.v[i].v<<i)
        return out

    def __getitem__(self, idx):
        return self.v[idx]

    def __setitem__(self, idx, bit):
        self.v[idx] = bit

    def __str__(self):
        vals = [str(v) for v in reversed(self.v)]
        out = ""
        for i,v in enumerate(vals):
            if i%4 == 0 and i>0:
                out += "."
            out += v
        return out

class Unit(object):
    def __init__(self, name="_"):
        self.id = name

    def update_modules(self):
        raise Exception("{}: update_modules not implemented".format(self.id))

    def tick(self):
        raise Exception("{}: tick not implemented".format(self.id))

    def tock(self):
        raise Exception("{}: tock not implemented".format(self.id))

    def __str__(self):
        return "{}: ---".format(self.id)
