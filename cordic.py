import math
import random

class CORDIC(object):
    def __init__(self, numFactors):
        self.angles = list()
        self.numFactors = 32
        self.k = 1

        for k in range(self.numFactors):
            self.k = self.k*(1/math.sqrt(1 + math.pow(2,-2*k)))

        for i in range(self.numFactors):
            self.angles.append(math.atan(1/(2**i)))

    def calcAngle(self, factors):
        s = 0
        for i,f in enumerate(factors):
            s = s + self.angles[i]*f
        return s

    def calcFactors(self, phi):
        phi = phi - (2*math.pi*int(phi/2/math.pi))
        pointer = 0
        factors = []
        angle = 0

        for a in self.angles:
            currentAngle = self.calcAngle(factors)
            if currentAngle < phi:
                factors.append(1)
            else:
                factors.append(-1)

        return factors

    def cosine_sine(self, phi):
        factors = c.calcFactors(phi)

        x = 1
        y = 0
        for i,f in enumerate(factors):
            shift = math.pow(2,-i)
            x = x - (f * y * shift)
            y = (f * x * shift) + y

        return self.k * x, self.k * y


if __name__ == "__main__":

    c = CORDIC(32)

    def test(num, epsilon=10**-8):
        for _ in range(num):
            phi = random.random()*math.pi/2

            factors = c.calcFactors(phi)
            angle = c.calcAngle(factors)

            diff = abs(phi - angle)
            if diff > epsilon:
                print("test failed for phi={}, difference={}".format(phi, diff))

    test(500)
#    for _ in range(3):
#        x = random.random() * math.pi/4
#
#        cosine, cordic_result = c.cosine_sine(x)
#        math_result = math.sin(x)
#
#        print("cordic: {}, math: {}".format(cordic_result, math_result))
