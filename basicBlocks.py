from base import Unit
from gates import *

class FullAdder(Unit):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.id == "_":
            self.id = "FullAdder"
# input
        self.a = Bit()
        self.b = Bit()
        self.cin = Bit()
# output
        self.cout = Bit()
        self.sum = Bit()
# intermediate
        self.propagate = Bit()

# modules
        self.xor_propagate = XOR(name="fa_xor_prop")
        self.xor_sum = XOR(name="fa_xor_sum")

        self.ao22 = AO22(name="fa_ao22")

        self.update_modules()

    def update_modules(self):
        self.xor_propagate.a = self.a
        self.xor_propagate.b = self.b
        self.xor_propagate.update_modules()
        self.propagate = self.xor_propagate.out

        self.xor_sum.a = self.propagate
        self.xor_sum.b = self.cin
        self.xor_sum.update_modules()
        self.sum = self.xor_sum.out

        self.ao22.a = self.a
        self.ao22.b = self.b
        self.ao22.c = self.propagate
        self.ao22.d = self.cin
        self.ao22.update_modules()
        self.cout = self.ao22.out

    def tick(self):
        self.xor_propagate.tick()
        self.xor_sum.tick()
        self.ao22.tick()

    def tock(self):
        self.xor_propagate.tock()
        self.xor_sum.tock()
        self.ao22.tock()

    def __str__(self):
        out = "{:<20}: inputs: a={}, b={}, cin={}\n".format(self.id, self.a.v, self.b.v, self.cin.v)
        out += "{:<20}: outputs: cout={}, sum={}".format("", self.cout.v, self.sum.v)
        return out

class FullAdderM1(Unit):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.id == "_":
            self.id = "FullAdderM1"
# input
        self.a = Bit()
        self.b = Bit()
        self.cin = Bit()
# output
        self.cout_not = Bit()
        self.sum = Bit()
# intermediate
        self.propagate = Bit()

# modules
        self.xor_propagate = XOR(name="fa_m1_xor_prop")
        self.xor_sum = XOR(name="fa_m1_xor_sum")

        self.aoi22 = AOI22(name="fa_m1_aoi22")

        self.update_modules()

    def update_modules(self):
        self.xor_propagate.a = self.a
        self.xor_propagate.b = self.b
        self.xor_propagate.update_modules()
        self.propagate = self.xor_propagate.out

        self.xor_sum.a = self.propagate
        self.xor_sum.b = self.cin
        self.xor_sum.update_modules()
        self.sum = self.xor_sum.out

        self.aoi22.a = self.a
        self.aoi22.b = self.b
        self.aoi22.c = self.propagate
        self.aoi22.d = self.cin
        self.aoi22.update_modules()
        self.cout_not = self.aoi22.out

    def tick(self):
        self.xor_propagate.tick()
        self.xor_sum.tick()
        self.aoi22.tick()

    def tock(self):
        self.xor_propagate.tock()
        self.xor_sum.tock()
        self.aoi22.tock()

    def __str__(self):
        out = "{:<20}: inputs: a={}, b={}, cin={}\n".format(self.id, self.a.v, self.b.v, self.cin.v)
        out += "{:<20}: outputs: cout_not={}, sum={}".format("", self.cout_not.v, self.sum.v)
        return out

class FullAdderM2(Unit):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.id == "_":
            self.id = "FullAdderM2"
# input
        self.a = Bit()
        self.b = Bit()
        self.cin_not = Bit()
# output
        self.cout = Bit()
        self.sum = Bit()
# intermediate
        self.t1 = Bit()
        self.propagate_not = Bit()

# modules
        self.nand = NAND()
        self.oai21_1 = OAI21(name="fa_m2_oai21_1")
        self.xor_sum = XOR(name="fa_m2_xor_sum")
        self.oai21_2 = OAI21(name="fa_m2_oai21_2")

        self.update_modules()

    def update_modules(self):
        self.nand.a = self.a
        self.nand.b = self.b
        self.nand.update_modules()
        self.t1 = self.nand.out

        self.oai21_1.a = self.a
        self.oai21_1.b = self.b
        self.oai21_1.c = self.t1
        self.oai21_1.update_modules()
        self.propagate_not = self.oai21_1.out

        self.oai21_2.a = self.propagate_not
        self.oai21_2.b = self.cin_not
        self.oai21_2.c = self.t1
        self.oai21_2.update_modules()
        self.cout = self.oai21_2.out

        self.xor_sum.a = self.propagate_not
        self.xor_sum.b = self.cin_not
        self.xor_sum.update_modules()
        self.sum = self.xor_sum.out

    def tick(self):
        self.nand.tick()
        self.oai21_1.tick()
        self.oai21_2.tick()
        self.xor_sum.tick()

    def tock(self):
        self.nand.tock()
        self.oai21_1.tock()
        self.oai21_2.tock()
        self.xor_sum.tock()

    def __str__(self):
        out = "{:<20}: inputs: a={}, b={}, cin_not={}\n".format(self.id, self.a.v, self.b.v, self.cin_not.v)
        out += "{:<20}: outputs: cout={}, sum={}".format("", self.cout.v, self.sum.v)
        return out

if __name__ == "__main__":
    def testFullAdder():
        fa = FullAdder()

        def ticktock():
            fa.tick()
            fa.tock()

        for _ in range(10):
            ticktock()

        def p(num):
            print("--- #{:<3} ---".format(num))
            print(fa)

        for a in [0,1]:
            for b in [0,1]:
                for c in [0,1]:
                    fa.a.v = a
                    fa.b.v = b
                    fa.cin.v = c

                    cycles=4
#                    p(0)
                    for _ in range(cycles):
                        ticktock()
                    p(cycles)

    testFullAdder()
